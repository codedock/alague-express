(function() {

	window.QuestionForm = Backbone.View.extend({
		
		events: {
			"submit": "postQuestion",
			"blur textarea.question": "handleCharLeft",
			"keyup textarea.question": "handleCharLeftAndButton",
		},

		opts: {
			urlPost: "/ask",
		},

		initialize: function(opts) {
			this.$question = this.$("textarea.question");
			this.$button = this.$("button.btn-ask");
			this.$infoCharleft = this.$(".info-charleft");

			this.$question.autogrow({
				speed: 0
			});

			this.opts.urlPost = this.$el.attr("action");
		},

		postQuestion: function(e) {
			e.preventDefault();
			
			var View = this;
			var $textarea = this.$("textarea.question");
			var question = $textarea.val();
			var questionTo = this.$el.data("user");
			var url = this.opts.urlPost;

			if(!question) {
				alert("Pertanyaan tidak boleh kosong")
				return false;
			}

			$.post(url, {question: question, userTo: questionTo})
			.done(function() {
				View.resetForm();
			})
			.fail(function() {
				alert("Something went wrong");
			});
		},

		resetForm: function() {
			this.$infoCharleft.text(this.$question.attr("maxlength"));
			this.$question.val('');
			this.$question.blur();
		},

		handleCharLeft: function(e) {
			var charLimit = this.$question.attr("maxlength");
			var questionLength = this.$question.val().length;

			var charLeft = parseInt(charLimit) - questionLength;

			this.$infoCharleft.text(charLeft);
		},

		handleCharLeftAndButton: function(e) {
			var questionLength = this.$question.val().length;

			this.handleCharLeft(e);

			if(questionLength > 0) {
				this.$button.removeAttr("disabled");
			} else {
				this.$button.attr("disabled", "disabled");
			}
		}

	});

})()