
// underscore configuration
if(_) {
	function tpl(template_id) {
		var html = $("#"+template_id).html();
		return _.template(html);
	}
}

function relogin() {
	window.location.href = "/logout";
}