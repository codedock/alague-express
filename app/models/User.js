var bcrypt = require("bcryptjs");
var SALT_FACTOR = 10;

module.exports = {

	// collection name
	collection: "users",

	// mongoose schema; http://mongoosejs.com/docs/guide.html
	schema: {
		username: {
			type: String,
			required: true
		},
		password: {
			type: String,
			required: true
		},
		avatar: {
			type: String,
			required: false
		},
	},

	// mongoose static methods: http://mongoosejs.com/docs/guide.html#statics
	statics: {
		
		register: function(user_data, callback) {
			var salt = bcrypt.genSaltSync(SALT_FACTOR);
			var	hashed_password = bcrypt.hashSync(user_data.password, salt);

			user_data.password = hashed_password;

			if(!user_data.username.match(/^[a-zA-Z0-9]+$/)) {
				var error = new Error("Username hanya boleh berupa kombinasi huruf dan angka");
				error.type = "register.failed";

				return callback(error, null);
			}

			this.findByUsername(user_data.username, function(err, user) {
				if(user) {
					err = new Error("Username sudah digunakan");
					err.type = "register.failed";
					
					callback(err, null);
				} else {
					User.create(user_data, callback);
				}
			});
		},

		findByUsername: function(username, callback) {
			this.findOne({'username': username}, callback);
		}

	},

	// mongoose model methods: http://mongoosejs.com/docs/guide.html#methods
	methods: {
		passwordMatch: function(password) {
			return bcrypt.compareSync(password, this.password);
		}
	}

};