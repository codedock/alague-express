module.exports = {

	pageRegister: function(req, res, next) {
		return res.render("theme/pages/register", {title: 'Register'});
	},

	register: function(req, res, next) {

		if(req.body.password != req.body.confirm_password) {
			res.flash("error", "password and confirm password not match");
			return res.redirect("/register");
		}

		var user_data = {
			username: req.body.username,
			password: req.body.password,
			avatar: (Math.round(Math.random()*10)+1)+'.jpg' // dummy avatar
		};

		User.register(user_data, function(err, user) {
			if(err) {
				if(err.type == "register.failed") {
					res.flash("error", err.message);
					res.redirect("/register");
				} else {
					next(err);
				}
			} else {
				res.flash("info", "Akun berhasil dibuat");
				res.redirect("/login");
			}
		});
	},

	pageUser: function(req, res, next) {
		var username = req.param('username');

		console.log(username);

		User.findByUsername(username, function(err, user) {
			if(err) return next(err);

			if(!user) return res.notFound();

			delete user.password;

			var data = {
				viewed_user: user,
				title: user.username,
			};

			res.render("theme/pages/questions", data);
		});
		
	},

	pageQuestions: function(req, res, next) {
		var data = {
			viewed_user: req.user,
			data: 'Pertanyaan'
		};

		res.render("theme/pages/questions", data);
	},

	pageAnswers: function(req, res, next) {
		var data = {
			viewed_user: req.user,
			title: 'Jawaban'
		};
		
		res.render("theme/pages/answers");
	},

};