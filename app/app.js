/**
 * APPLICATION CORE FILES
 * ---------------------------------
 * You don't need to modify this
 * Except you now what you are doing
 */

var fs = require("fs"),
		Express = require('express'),
		express = Express(),
		server = require('http').Server(express),
		_ = require("lodash"),
		os = require("os"),
		util = require("util"),
		bodyParser = require("body-parser"),
		debug =	require("debug")('app:core');


var default_config_starter = {
	public: __dirname+"/../public",
	views: __dirname+"/views",
	controllers: __dirname+"/controllers",
	models: __dirname+"/models",
	configs: __dirname+"/configs",
	libraries: __dirname+"/libraries",
	helpers: __dirname+"/helpers",
	environments: {}
};

var EventHandler = function() {

	var events = {};

	function isMatchEvent(called_event, event_name) {
		if(called_event == event_name) return true;

		// if event_name end with *, 
		if(event_name.match(/\.\*$/)) {
			// remove that, for regex
			event_regex_str = event_name.replace(/\.\*$/, '');
			event_regex_str = event_name.replace('.', '\\.');
			
			var regex = new RegExp("^"+event_regex_str);

			return called_event.match(regex)? true : false;
		} else {
			return false;
		}
	}

	this.on = function(evt, callback) {
		if(typeof callback != "function") {
			throw new Error("Event callback must be function");
		}

		if(!events[evt]) {
			events[evt] = [];
		}

		events[evt].push(callback);
	};

	this.fire = function(evt) {
		for(evt_key in events) {
			if(isMatchEvent(evt, evt_key)) {
				for(i in events[evt_key]) {
					events[evt_key][i].apply(null, arguments);
				}
			}
		}
	};

}

function getConnectionString(data) {
	var authorize = "";
	var host_and_port = data.host;
	
	if(data.username && data.password) {
		authorize += data.username+":"+data.password+"@";
	}

	if(data.port) {
		host_and_port += ":"+data.port;
	}

	return "mongodb://"+authorize+host_and_port+"/"+data.database;
}

function getConfigs(dir, excepts, emptyIfNotExists) {
	if(emptyIfNotExists === true && fs.existsSync(dir) === false) {
		return {};
	}

	var files = fs.readdirSync(dir);

	var configs = {};
	for(i in files) {
		var file = files[i];
		if(file.match(/\.js$/)) {
			var config_path = dir+'/'+file;
			var config_name = file.replace(/\.js$/, '');
			if(util.isArray(excepts) && excepts.indexOf(config_name) > -1) continue;

			configs[config_name.toLowerCase()] = require(config_path);
		}
	}

	return configs;
}

function getConfigOnly(dir, config_name) {
	var config_filepath = dir+"/"+config_name+".js";

	if(fs.existsSync(config_filepath) === false) {
		return {};
	} else {
		return require(config_filepath);
	}
}

function loadLibraries(app) {
	var dir = app._configs.starter.libraries;
	var files = fs.readdirSync(dir);

	for(i in files) {
		var file = files[i];
		if(file.match(/\.js$/)) {
			var lib_path = dir+'/'+file;
			var lib_name = file.replace(/\.js$/, '');

			debug("Load library "+lib_name);
			global[lib_name] = require(lib_path);
		}
	}
}

function loadControllers(app) {
	var controllers = {};
	var dir = app._configs.starter.controllers;
	var files = fs.readdirSync(dir);

	var controllers = {};
	for(i in files) {
		var file = files[i];
		if(file.match(/\.js$/)) {
			var ctrl_path = dir+'/'+file;
			var ctrl_name = file.replace(/\.js$/, '');
			controllers[ctrl_name] = require(ctrl_path);
			debug("Load controller "+ctrl_name);
		}
	}

	global.Controller = controllers;
}

function loadModels(app) {
	if(app.config("app.database") !== true) {
		debug("Models not loaded, database has been disabled in app configuration");
		return;
	}

	var dir = app._configs.starter.models;
	var files = fs.readdirSync(dir);

	for(i in files) {
		var file = files[i];
		if(file.match(/\.js$/)) {
			var model_path = dir+'/'+file;
			var model_name = file.replace(/\.js$/, '');
			var model_data = require(model_path);

			model_data = _.extend({
				collection: undefined,
				shardKey: undefined,
				strict: false,
				schema: {},
				methods: {},
				statics: {},
			}, model_data);

			var model_schema = model_data.schema;
			var methods = {};
			var model_schema_options = {
				strict: model_data.strict,
				collection: model_data.collection,
				shardKey: model_data.shardKey	
			};

			model_schema = app.mongoose.Schema(model_schema, model_schema_options);

			if(global[model_name] !== undefined) {
				throw new Error("Cannot initialize model "+model_name+" : object name already used, use another name");
			} else {
				
				for(key in model_data.methods) {
					model_schema.methods[key] = model_data.methods[key];
				}

				for(key in model_data.statics) {
					model_schema.statics[key] = model_data.statics[key];
				}

				debug("Load model "+model_name);
				global[model_name] = app.mongoose.model(model_name, model_schema);
			}
		}
	}
}

function initializeConfigs(app, excepts) {
	var configs = getConfigs(app._configs.starter.configs, excepts);
	var configEnvDir = app.environment()? app._configs.starter.configs+'/'+app.environment() : null;
	var configEnvironment = getConfigs(configEnvDir, excepts, true);

	app._configs.starter.configsEnv = configEnvDir;
	app._configs.default = configs;
	app._configs.environment = configEnvironment;

	app.configs = {};

	for(keyfile in app._configs.default) {
		var fileconfigs = _.extend(
			_.clone(app._configs.default[keyfile], true), 
			_.clone(app._configs.environment[keyfile] || {}, true)
		);

		app.configs[keyfile] = fileconfigs;
	}

	for(keyfile in app._configs.environment) {
		if(!app.configs[keyfile]) {
			app.configs[keyfile] = _.clone(app._configs.environment[keyfile], true)
		}
	}

	app.config = function(key, default_value) {
		var keysplit = key.split(".");
		var value = default_value;
		var __configs = app.configs;

		for(i in keysplit) {
			var k = keysplit[i];
			if(i == 0) {
				if(__configs[k] != undefined) {
					value = __configs[k];
				} else {
					value = default_value;
				}
			} else {
				if(typeof value == "object" && value[k] != undefined) {
					value = value[k];
				} else {
					value = default_value;
				}
			}
		}

		return value;
	};
}

function setupViewEngine(app) {
	app.express.engine('jade', require('jade').__express);
	app.express.set('views', app._configs.starter.views);
	app.express.set('view engine', 'jade');

	app.express.use(function(req, res, next) {
		app.express.locals.App = app;
		app.express.locals.req = req;
		app.express.locals.res = res;
		next();
	});
}

function setupSession(app) {
	var sessionConfig = app.config("session");

	if(app.config("app.session")) {
		var session = require('express-session');

		// if socket and session enabled, we need session store to share session into socket
		if(!sessionConfig.sessionStore && app.config("app.socket")) {
			sessionConfig.store = new session.MemoryStore();
		}
		
		app.sessionStore = sessionConfig.store;

		app.express.use(session(sessionConfig));
		
		// simple flash middleware
		app.express.use(function(req, res, next) {

			req._flash = express.locals.flash = _.clone(req.session.flash, true);
			req.session.flash = {};
			
			// get flash
			req.flash = function(key) {
				return req._flash[key];
			};

			// set flash
			res.flash = function(key, value) {
				req.session.flash[key] = value;
			};

			next();
		});

		debug("Setup session");
	} else {
		debug("Session has been disabled in app configuration");
	}
}

function connectDatabaseAndRunApp(app, callback) {

	app.event.fire("connect.database.before");

	app.connectionString = getConnectionString(app.configs.database).replace(/\/+$/, '');

	app.mongoose = require('mongoose');
	app.mongoose.connect(app.connectionString);
	app.db = app.mongoose.connection;

	app.db.on("error", function() {
		app.event.fire("connect.database.error");
		app.event.fire("connect.database.after");
		throw new Error("Failed to connect "+app.connectionString);
	});

	app.db.once("open", function() {
		global.MongooseTypes = app.mongoose.Schema.Types;
		
		console.log("Sucessfully connect to "+app.connectionString);
		app.event.fire("connect.database.open");
		app.event.fire("connect.database.after");

		debug("Connect to "+app.connectionString);

		app.event.fire("load.model.before");
		loadModels(app);
		app.event.fire("load.model.after");

		runApp(app, callback);
	});
}

function runApp(app, callback) {
	var port = app.config("app.port", 3000);
	console.log("App run on http://localhost:"+port);
	server.listen(port, function() {
		app.event.fire("run");
		if(typeof callback == "function") callback();
	});
}

function loadEvents(app) {
	var dirConfigs = app._configs.starter.configs;
	var dirConfigsEnv = app._configs.starter.configsEnv;

	var defEvents = getConfigOnly(dirConfigs, "events");
	var envEvents = getConfigOnly(dirConfigsEnv, "events");

	app._configs.default.events = defEvents;
	app._configs.environment.events = envEvents;

	var events = _.extend(_.clone(defEvents, true), _.clone(envEvents, true));

	app.event = new EventHandler();

	for(evt in events) {
		app.event.on(evt, events[evt]);
	}
}

function loadRoutes(app) {
	var dirConfigs = app._configs.starter.configs;
	var dirConfigsEnv = app._configs.starter.configsEnv;

	var defRoutes = getConfigOnly(dirConfigs, "routes");
	var envRoutes = getConfigOnly(dirConfigsEnv, "routes");

	app._configs.default.routes = defRoutes;
	app._configs.environment.routes = envRoutes;

	var routes = _.extend(_.clone(defRoutes, true), _.clone(envRoutes, true));
	var parsed_routes = [];

	app.configs.routes = routes;

	for(route in routes) {
		var action = routes[route];
		var split_route = route.split(' ');

		var r = {
			method: split_route[0],
			path: split_route[1],
			middlewares: [],
			action: null
		};

		if(typeof action == "function") {
			var actions = [action];
		} else {
			var actions = action;
		}

		r.action = actions.pop();

		actions.forEach(function(middleware) {
			if(typeof middleware != 'function') {
				throw new Error("Route middleware must be function, not "+(typeof middleware));
			}
			app.express[r.method.toLowerCase()](r.path, middleware);
		});

		if(typeof r.action != 'function') {
			throw new Error("Route middleware must be function, not "+(typeof r.action));
		}
		app.express[r.method.toLowerCase()](r.path, r.action);

		parsed_routes.push(r);
	}

	app.getRoutes = function() {
		return parsed_routes;
	};
}

// share express session to socket server :
// http://stackoverflow.com/questions/24679046/expressjssocket-ioexpress-session
function initializeSocketSession(socket, next) {
	var COOKIE_NAME = global.App.configs.session.name;
	var COOKIE_SECRET = global.App.configs.session.secret;
	var cookie = require("cookie");
	var cookieParser = require("cookie-parser");
	var sessionStore = global.App.sessionStore;

	try {
		var data = socket.handshake || socket.request;
		
		if (! data.headers.cookie) {
			return next(new Error('Missing cookie headers'));
		}


		//console.log('cookie header ( %s )', JSON.stringify(data.headers.cookie));

		var cookies = cookie.parse(data.headers.cookie);

		//console.log('cookies parsed ( %s )', JSON.stringify(cookies));

		if (! cookies[COOKIE_NAME]) {
			return next(new Error('Missing cookie ' + COOKIE_NAME));
		}
		
		var sid = cookieParser.signedCookie(cookies[COOKIE_NAME], COOKIE_SECRET);
		
		if (! sid) {
			return next(new Error('Cookie signature is not valid'));
		}

		//console.log('session ID ( %s )', sid);
		data.sid = sid;

		//console.log(global.App.sessionStore);

		global.App.sessionStore.get(sid, function(err, session) {
			if (err) return next(err);

			//console.log(session);
			socket.session = session || {};
			next();
		});
	} catch (err) {
		console.error(err.stack);
		next(new Error('Internal server error'));
	}
}

function loadSockets(app) {
	if(app.config("app.socket") !== true) {
		debug("Socket has been disabled in app configuration");
	}

	app.io = require('socket.io')(app.server);

	if(app.config("app.session")) {
		app.io.use(initializeSocketSession);
	}

	app.event.fire("load.socket.init");

	var dirConfigs = app._configs.starter.configs;
	var dirConfigsEnv = app._configs.starter.configsEnv;

	var defSockets = getConfigOnly(dirConfigs, "socket");
	var envSockets = getConfigOnly(dirConfigsEnv, "socket");

	app._configs.default.socket = defSockets;
	app._configs.environment.socket = envSockets;

	app.configs.socket = _.extend(_.clone(defSockets, true), _.clone(envSockets, true));

	app.io.on('connection', function(socket) {

		if(typeof app.configs.socket.connection == 'function') {
			app.configs.socket.connection.apply(null, [socket]);
		}

		for(evt in app.configs.socket) {
			if(evt == 'connection') continue;

			debug('register socket event '+evt);
			socket.on(evt, app.configs.socket[evt]);
		}

	});
}

function detectEnvironment(app) {
	var hostname = os.hostname();
	var enviroments = app._configs.starter.environments || {};

	for(env in enviroments) {
		var envs = enviroments[env];
		if(envs.indexOf(hostname) > -1) return env;
	}

	return null;
}

var errorHttpHandler = function(req, res, next) {
	res.notFound = function(data) {
		return App.event.fire("route.error.404", req, res, next, data);
	};

	res.abort = function(data) {
		return App.event.fire("route.error.500", req, res, next, data);
	};

	next();
}

var App = function Application(config_starter) {
	global.App = this;
	global.Controller = {};
	global._ = _;

	var This = this;

	this._configs = {
		starter: {},
		environment: {},
		default: {}
	}

	this._configs.starter = _.extend(default_config_starter, config_starter);
	this.express = express;
	this.server = server;
	this.routes = {};

	var environment = detectEnvironment(this);

	this.environment = function() {
		return environment;
	};

	this.express.use(Express.static(this._configs.starter.public));
	
	initializeConfigs(this, ["routes", "events", "socket"]);
	loadEvents(this);

	this.event.fire("load.libraries.before");
	loadLibraries(this);
	this.event.fire("load.libraries.after");

	this.event.fire("load.controller.before");
	loadControllers(this);
	this.event.fire("load.controller.after");

	this.event.fire("setup.session.before")
	setupSession(this);	
	this.event.fire("setup.session.after");

	this.event.fire("setup.view.before");
	setupViewEngine(this);
	this.event.fire("setup.view.after");

	this.event.fire("load.socket.before");
	loadSockets(this);
	this.event.fire("load.socket.after");

	this.express.use(bodyParser.json()); // for parsing application/json
	this.express.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
	
	this.express.use(errorHttpHandler);

	this.event.fire("load.routes.before");
	loadRoutes(this);
	this.event.fire("load.routes.after");

	/*** NOT FOUND HANDLER ***/
	// http://www.hacksparrow.com/express-js-custom-error-pages-404-and-500.html
	this.express.use(function(req, res, next) {
		res.status(404);
		debug("Call to undefined route "+req.url);
		This.event.fire("route.error.404", req, res, next);
	});

	this.express.use(function(error, req, res, next) {
		debug(error);

		res.status(500);
		This.event.fire("route.error.500", error, req, res, next);
	});
	/*** END - NOT FOUND HANDLER ***/

	this.run = function(cb) {
		this.event.fire("run.before");
		if(this.config("app.database") === true) {
			connectDatabaseAndRunApp(this, cb);
		} else {
			runApp(this, cb);
		}
	}
	
};

module.exports = function(configs) {
	var app = new App(configs);

	return app;
};