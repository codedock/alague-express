/* express-session config
 * ----------------------
 * for more configuration, you can see at:
 * https://github.com/expressjs/session#options
 */

module.exports = {

	name: 'appsess',

	secret: 'y0uR 5eCr3t k3Y',
	
	resave: false,
	
	saveUninitialized: true

};