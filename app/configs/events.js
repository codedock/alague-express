/**
 * APPLICATION EVENTS
 * ------------------------------
 * here you can define app events
 */


module.exports = {

	// inject express middleware for set global user variable
	"load.routes.before": function(event) {
		App.express.use(function(req, res, next) {
			App.express.locals.user = req.user = req.session.user || null;
			next();
		});
	},

	// error 404 handler
	"route.error.404": function(event, req, res, next) {
		res.status(404).send("Error 404: Page not found");
	},

	// error 500 handler
	"route.error.500": function(event, error, req, res, next) {
		res.status(500).send("Error 500: Something went wrong");
		next(error);
	}

}