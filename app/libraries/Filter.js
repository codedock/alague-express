module.exports = {

	routeLoggedUser: function(req, res, next) {
		if(!req.user) {
			res.flash("Login dulu plis");
			res.redirect("/login");
		} else {
			next();
		}
	},

	routeAjax: function(req, res, next) {
		if(!req.xhr) {
			return res.status(400).json({ error: 'Bad request' });
		} else {
			next();
		}
	}

};