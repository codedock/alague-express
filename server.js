
var App = require("./app/app")({

	// public directory
	public: __dirname+"/public",

	// configs path
	configs: __dirname+"/app/configs",

	// controllers path
	controllers: __dirname+"/app/controllers",

	// views path
	views: __dirname+"/app/views",

	// models path
	models: __dirname+"/app/models",
	
	// environment system (for configs)
	environments: {
		"dev": ["foo"] // << "foo" = computer name, you can add your own here
	}

});

App.run();